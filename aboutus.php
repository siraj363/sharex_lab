<?php
include './header/topbar.php';
include './header/header.php';
?>
<!-- ======= About Us Section ======= -->
<section id="about" class="about padding-top-130">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>About Us</h2>
            <p style="text-align: justify;">
                SHAREX Laboratories Ltd. is a specialty pharmaceutical company focused on the development and commercialization of innovative medical treatments utilizing its proprietary drug delivery systems.

            </p>
            <p style="text-align: justify;">
                The Company's primary emphasis is on advancing its pharmaceutical technology platform for site-specific and systemic drug delivery. Designed to release drugs over a period of hours, days, weeks or even months. A variety of injectible and implantable applications are undergoing feasibility testing to address major unmet needs in pain management, inflammation, oncology, ophthalmology, device coating and other medical fields.
            </p>
        </div>

        <div class="row">
            <div class="col-lg-6" data-aos="fade-right" style="background-image: url('assets/img/company.jpg'); background-size: cover; background-repeat: no-repeat; background-position: center; height: 500px;">
                <!--<img src="assets/img/company.jpg" class="img-fluid" alt="">-->
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left">
                <h6>Production Activities</h6>
                <p class="font-italic f-13">
                    Our factory is well equipped with modern machines and equipments to manufacture high quality Injectables, Tablets, Medicinal Syrups, Opthalmics and Galenicals.
                </p>
                <p class="font-italic f-13">
                    For manufacturing best quality Pharmaceutical products, Sharex confine to the G.M.P. Provisions, as laid down under Pakistan Drug Rules 1976.
                </p>
                <h6>Quality Control Department</h6>
                <p class="font-italic f-13">
                    Staffed by flight brains and equipped with the advanced test apparatus, our quality control department ensure the manufacture of reliable products by means of multistage close examination system verifying from raw-material, in process to final packaging.
                </p>
                <h6>National Network</h6>
                <p class="font-italic f-13">
                    We have our offices spreading all over the country and have an excellent net work of distribution.
                </p>
                <p class="font-italic f-13">
                    Detailmen at our branch offices provide useful drug information services to physicians and other health care professionals. Personal selling efforts are coordinated and supported by proper advertising campaigns and other sales promotion activities such as setting up of store displays, participating in trade shows and exhibitions and using samples or premiums.
                </p>
                <h6>Marketing & Sales Promotion</h6>
                <p class="font-italic f-13">
                    Our marketing department manage sales very systematically. Quarter wise sales conference are regularly held to make a through analysis of company’s existing situations its markets, its competition, the products, the distribution channels and the promotional programes. The process continues with management determining the objectives, selecting the appropriate strategies and tactics and then periodically evaluating the operating results.
                </p>
            </div>
            <div class="col-lg-12 pt-4 content">
                <p class="font-italic">
                    We know that congenial atmosphere in our factory has a lot to do with the quality of our products. A cooperative attitude and a spirit of devotion are the essence of any organization. Therefore, Sharex Laboratories Ltd., care for the welfare and health of its employees by providing:-
                </p>
                <ul>
                    <li><i class="icofont-check-circled"></i> Medical coverage to them and to their families.</li>
                    <li><i class="icofont-check-circled"></i> Supporting various sports and hobby clubs.</li>
                    <li><i class="icofont-check-circled"></i> Offering loan for various purposes and free education to its employee’s children.</li>
                    <li><i class="icofont-check-circled"></i> Bonuses.</li>
                    <li><i class="icofont-check-circled"></i> Gratuity.</li>
                    <li><i class="icofont-check-circled"></i> Temporary disability allowance and;</li>
                    <li><i class="icofont-check-circled"></i> Miscellaneous social security benefits are also offered.</li>
                    <li><i class="icofont-check-circled"></i> Three workers from employees are sent every year for Pilgrimage of Haj to Mecca in Saudi Arabia.</li>
                </ul>
            </div>
        </div>

    </div>
</section><!-- End About Us Section -->
<?php
include './footer/footer.php';
?>
