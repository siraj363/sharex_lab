<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

if (!isset($_POST['final_data'])) {
    header('location: index.php');
}
require './vendor/phpmailer/phpmailer/src/Exception.php';
require './vendor/phpmailer/phpmailer/src/PHPMailer.php';
require './vendor/phpmailer/phpmailer/src/SMTP.php';

include_once './classes/Crud.php';
$crud = new Crud();

//give a filename
$filename = "order.csv";
$handle = fopen("order.csv", "w");
//set the column names
$cells[] = array();
$cells_string = fputcsv($handle, array('Sr#','Product Name','Quantity'));
//pass all the form values
foreach ($_POST['final_data'] as $record)
{
    fputcsv($handle, array($record['sr'], $record['product'], $record['quantity']));
}
fclose($handle);

$mail = new PHPMailer(true);
//$mail->addStringAttachment($csv_string,$fileName);

try {
    //Server settings
//    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $mail->SMTPDebug = 1;                      // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'frecourses.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'admin@frecourses.com';                    // SMTP username
    $mail->Password   = 'Ayeshahashim03023730715';                               // SMTP password
    $mail->SMTPSecure = 'ssl';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('info@frecourses.com', 'Information');
    $mail->addAddress('admin@frecourses.com','Admin');     // Add a recipient
//    $mail->addAddress('ellen@example.com');               // Name is optional
    $mail->addReplyTo($_POST['email'], 'Orderer');
//    $mail->addCC('cc@example.com');
//    $mail->addBCC('bcc@example.com');

    // Attachments
    $mail->addAttachment($filename);         // Add attachments
//    $mail->addAttachment('order.csv');    // Optional name

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Order from '.$_POST['name'];
    $mail->Body    = 'Name: '.$_POST['name'].'<br>Phone: '.$_POST['phone'] . '<br>Address: '.$_POST['address'].'<br>Message: '.$_POST['message'].'';
//    $mail->AltBody = 'This is message section';

    if($mail->send()){
        return true;
//        header('location: index.php?mailsent=true');
    }
    echo 'Message has been sent';
} catch (Exception $e) {
    return false;
//    die('hereinexception');
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
?>