<?php
include './header/topbar.php';
include './header/header.php';
?>
<main id="main">
    <!-- ======= Doctors Section ======= -->
    <section id="bankers" class="counts paddiing-top-sections">
        <div class="container">
            <div class="section-title">
                <h2>Bankers</h2>
            </div>
            <div class="row no-gutters">

                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box bank-image-top">
                        <img class="bank-image" src="assets/img/mcb.png" style="width: 100%;"/>
                        <div class="middle-btn">
                            <button data-toggle="modal" data-target="#mcb_modal" class="btn btn-primary custom-darkred-btn">View Detail</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box bank-image-top">
                        <img class="bank-image" src="assets/img/ubl.png" style="width: 100%;"/>
                        <div class="middle-btn">
                            <button data-toggle="modal" data-target="#ubl_modal" class="btn btn-primary custom-darkred-btn">View Detail</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box bank-image-top">
                        <img class="bank-image" src="assets/img/alfalah.jpg" style="width: 100%;"/>
                        <div class="middle-btn">
                            <button data-toggle="modal" data-target="#alfalah_modal" class="btn btn-primary custom-darkred-btn">View Detail</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box bank-image-top">
                        <img class="bank-image" src="assets/img/meezan.jpg" style="width: 100%;"/>
                        <div class="middle-btn">
                            <button data-toggle="modal" data-target="#meezan_modal" class="btn btn-primary custom-darkred-btn">View Detail</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box bank-image-top">
                        <img class="bank-image" src="assets/img/alhabib.jpg" style="width: 100%;"/>
                        <div class="middle-btn">
                            <button data-toggle="modal" data-target="#bank_al_habib_modal" class="btn btn-primary custom-darkred-btn">View Detail</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box bank-image-top">
                        <img class="bank-image" src="assets/img/hbl.png" style="width: 100%;"/>
                        <div class="middle-btn">
                            <button class="btn btn-primary custom-darkred-btn">No Detail</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box bank-image-top">
                        <img class="bank-image" src="assets/img/allied.jpg" style="width: 100%;"/>
                        <div class="middle-btn">
                            <button class="btn btn-primary custom-darkred-btn">No Detail</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-md-flex align-items-md-stretch">
                    <div class="count-box bank-image-top">
                        <img class="bank-image" src="assets/img/national.png" style="width: 100%;"/>
                        <div class="middle-btn">
                            <button class="btn btn-primary custom-darkred-btn">No Detail</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctors Section -->
</main>
<?php include './footer/footer.php'; ?>