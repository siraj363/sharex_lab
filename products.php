<?php

include './header/topbar.php';
include './header/header.php';
include_once './classes/Crud.php';


$query_cat = 'SELECT *
FROM categories';
$crud = new \Crud();
$categories = $crud->getData($query_cat);
?>
<main id="main">
    <section id="technologies" class="departments about paddiing-top-sections">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Commercialized Products</h2>
            </div>
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-3 mb-5 mb-lg-0">
                    <ul class="nav nav-tabs flex-column">
                        <?php
                        $count_li = 0;
                        foreach ($categories as $cat) {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link <?= $count_li == 0 ? 'active' : ''; ?>" data-toggle="tab" href="#tab-<?= $cat['id'] ?>">
                                    <h4><?= $cat['name'] ?></h4>
                                </a>
                            </li>
                            <?php
                            $count_li++;
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-lg-9">
                    <div class="tab-content content">
                        <?php
                        $count_cont = 0;
                        foreach ($categories as $cat) {
                            $query_prod = 'select * from products where cat_id=' . $cat['id'];
                            $data_record = $crud->getData($query_prod);
                            ?>
                            <div class="tab-pane <?= $count_cont == 0 ? 'active' : ''; ?>" id="tab-<?= $cat['id'] ?>">
                                <table id="products_table-<?= $cat['id'] ?>" class="products_tablee table table-hover table-striped table-bordered table-responsive-lg table-responsive-md table-responsive-sm">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th class="custom_th">Product</th>
                                            <th class="custom_th">Formulation</th>
                                            <!--<th class="custom_th" colspan="2">Presentation</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data_record as $value_prod) { ?>
                                            <tr>
                                                <td><?= $value_prod['brand_name'] ?></td>
                                                <td><?= str_replace(',', '<br>', $value_prod['formulation']) ?></td>
                                            </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                            <?php
                            $count_cont++;
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </section>
</main>
<?php include './footer/footer.php'; ?>