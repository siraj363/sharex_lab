<?php

include './header/topbar.php';
include './header/header.php';
?>
<!--<main id="main">-->
<!-- ======= Technologies Platforms Section ======= -->
    <section id="branches" class="departments about paddiing-top-sections">
        <div class="container">
            <div class="section-title">
                <h2>Branch Offices</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover table-striped table-bordered table-responsive-lg table-responsive-md table-responsive-sm">
                        <thead class="thead-dark">
                            <tr>
                                <th class="custom_th">Addresses</th>
                                <th class="custom_th">Phones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Nasir Mansion, Railway Road-2 Peshawar</td>
                                <td>091-2211694-2210167</td>
                            </tr>
                            <tr>
                                <td>Hikmat Manzil, Patiala Ground-5,Link Mcleod Road - Lahore</td>
                                <td>042-7352242-7463084</td>
                            </tr>
                            <tr>
                                <td>6-J, Civil Line, Jail Road- Faisalabad</td>
                                <td>041-2642652</td>
                            </tr>
                            <tr>
                                <td>360-Shamsabad, Hamayun Road -Multan</td>
                                <td>061-4513362-4560880</td>
                            </tr>
                            <tr>
                                <td>Shadab Center, Sokha Talab Sukkur <br>Abdul Latif Khawar</td>
                                <td>071-5614868-5612582</td>
                            </tr>
                            <tr>
                                <td>Old Machi Hut, Shahi Bazar- Hyderabad</td>
                                <td>022-2640377</td>
                            </tr>
                            <tr>
                                <td>P. 1024-D, Govt. Degree College Chowk, Asghar Mall Road <br> Rawalpindi</td>
                                <td>051-5536344</td>
                            </tr>
                            <tr>
                                <td>Matha Mal Street, Fatima Jinnah Road, Quetta</td>
                                <td>081-2 836639</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
<!--</main>-->
<?php include './footer/footer.php'; ?>