<?php
include './header/topbar.php';
include './header/header.php';
?>
<main id="main">
<!-- ======= Doctors Section ======= -->
    <section id="management" class="doctors section-bg paddiing-top-sections">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Management</h2>
                <!--<p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>-->
            </div>

            <div class="row">

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="member" data-aos="fade-up" data-aos-delay="100">
                        <div class="member-img">
                            <img src="assets/img/profile_default.png" class="img-fluid" alt="">
                            <div class="social">
                                <a href="mailto:amin@sharexlabs.com">amin@sharexlabs.com</a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4>M.Amin Bajwa</h4>
                            <span>President</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="member" data-aos="fade-up" data-aos-delay="200">
                        <div class="member-img">
                            <img src="assets/img/profile_default.png" class="img-fluid" alt="">
                            <div class="social">
                                <a href="mailto:farooq@sharexlabs.com">farooq@sharexlabs.com</a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4>Farooq Amin Bajwa</h4>
                            <span>Managing Director</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="member" data-aos="fade-up" data-aos-delay="300">
                        <div class="member-img">
                            <img src="assets/img/profile_default.png" class="img-fluid" alt="">
                            <div class="social">
                                <a href="mailto:usman@sharexlabs.com">usman@sharexlabs.com</a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4>Usman Amin Bajwa</h4>
                            <span>Director Production</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Doctors Section -->
</main>
<?php include './footer/footer.php'; ?>