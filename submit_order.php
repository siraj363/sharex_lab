<?php
//if (!isset($_POST['products'])) {
//    header('location: index.php');
//}
include './header/topbar.php';
include './header/header.php';
include_once './classes/Crud.php';
$crud = new Crud();
$array_post = $_POST;
?>
<!-- ======= Hero Section ======= -->
<!--<main id="main">-->
<section class="table paddiing-top-sections" id="order-submit">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Please Fill Order Sheet</h3>
                <form id="order_query_form">
                    <?php
                    foreach ($array_post as $key => $value) {
                        if (is_array($value)) {
                            foreach ($value as $key_ => $value_) {
                                ?>
                                <input type="hidden" name="<?= $key ?>[<?= $key_?>]" value="<?= $value_ ?>"/>
                                <?php
                            }
                        } else {
                            ?>
                            <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
                            <?php
                        }
                    }
                    ?>

                    <table class="table table-hover table-striped">

                        <thead>
                            <tr>
                                <th>Sr#</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            $arry_count = 0;
                            $products_array = [];
                            $quantities_array = [];
                            $final_data = [];
                            foreach ($_POST['products'] as $prod) {
                                $query = 'select * from products where id=' . $prod;
                                $data = $crud->getData($query);
                                ?>
                                <tr>
                                    <td><?= $count ?></td>
                            <input type="hidden" name="final_data[<?= $arry_count ?>][sr]" value="<?= $count ?>"/>

                            <td><input class="disabled_custom" type="text" name="final_data[<?= $arry_count ?>][product]" value="<?= $data[0]['brand_name'] ?>"/></td>
                            <td><input type="text" name="final_data[<?= $arry_count ?>][quantity]"/></td>
                            </tr>
                            <?php
                            $count++;
                            $arry_count++;
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <input name="submit" type="submit" value="Send Order"/>
                        </tfoot>
                    </table>
                </form>
            </div>
        </div>
    </div>
</section>
<!--</main>-->

<?php
include './footer/footer.php';
?>

