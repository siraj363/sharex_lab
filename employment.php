<?php
include './header/topbar.php';
include './header/header.php';
?>
<main id="main">
<!-- ======= Employment Section ======= -->
    <section id="employment" class="doctors section-bg paddiing-top-sections">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Current Job Opportunities</h2>
                <p>Sharex Laboratories, a recognized innovator in Pharmaceutical Industry for use primarily in ethical pharmaceutical products, is committed to the identification, development and commercialization of innovative systems for the pharmaceutical industry.</p>
            </div>
            <div class="row">
                <div class="col-lg-4" style="background-image: url('assets/img/employment.jpg'); background-size: cover; background-repeat: no-repeat; background-position: center;height: 270px;">
                </div>
                <div class="col-lg-8">
                    <h4>How To Apply</h4>
                    <p>To apply for a position, please mail, fax or e-mail your resume and cover letter to Sharex Laboratories Ltd.</p>
                    <div class="row">
                        <div class="col-lg-6">
                            <p>Mailing Address</p>
                        </div>
                        <div class="col-lg-6" style="padding-left: 0;">
                            <p>Fax: (+92) 68-5702853</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <p>Att: Human Resources</p>
                            <p>Sharex Laboratories (PVT) LTD.</p>
                            <p>Sharex Colony, Sadiqabad-Pakistan</p>
                        </div>
                        <div class="col-lg-6" style="padding-left: 0;">
                            <p>Email: <a href="mailto: jobs@sharexlabs.com">jobs@sharexlabs.com</a></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</main>
<?php include './footer/footer.php'; ?>