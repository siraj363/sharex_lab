<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

if (!isset($_POST['email'])) {
    header('location: index.php');
}
require './vendor/phpmailer/phpmailer/src/Exception.php';
require './vendor/phpmailer/phpmailer/src/PHPMailer.php';
require './vendor/phpmailer/phpmailer/src/SMTP.php';

include_once './classes/Crud.php';
$crud = new Crud();

$mail = new PHPMailer(true);
//$mail->addStringAttachment($csv_string,$fileName);

try {
    //Server settings
//    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $mail->SMTPDebug = 1;                      // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'sharexlabsltd@gmail.com';                    // SMTP username
    $mail->Password   = 'sharex0000';                               // SMTP password
    $mail->SMTPSecure = 'ssl';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('sharexlabsltd@gmail.com', 'Contact Query');
    $mail->addAddress('sharexlabs@gmail.com','Admin');     // Add a recipient
//    $mail->addAddress('ellen@example.com');               // Name is optional
    $mail->addReplyTo($_POST['email'], 'Orderer');
    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = $_POST['subject'];
    $mail->Body    = 'Name: '.$_POST['name'].'<br>Email: '.$_POST['email'] . '<br>Message: '.$_POST['message'];
//    $mail->AltBody = 'This is message section';

    if($mail->send()){
        return true;
//        header('location: index.php?mailsent=true');
    }
    return true;
} catch (Exception $e) {
    return false;
//    die('hereinexception');
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}
?>