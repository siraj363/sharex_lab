<?php

include './header/topbar.php';
include './header/header.php';
?>
<!--<main id="main">-->
<!-- ======= Technologies Platforms Section ======= -->
    <section id="technologies" class="departments about paddiing-top-sections">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Technologies & Platforms</h2>
            </div>
            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <ul class="nav nav-tabs flex-column">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#tab-1">
                                <h4>Parenteral Drug Delivery</h4>
                                <p>Sharex Laboratories is focused primarily on the development of its bioerodible polymers for parenteral drug delivery, both injectable and implantable. The Company has made substantial progress in developing these technologies, opening up prospects for significant improvements over existing approaches to drug delivery.</p>
                            </a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link" data-toggle="tab" href="#tab-2">
                                <h4>Oral Drug Delivery</h4>
                            </a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link" data-toggle="tab" href="#tab-3">
                                <h4>Topical</h4>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-8">
                    <div class="tab-content content">
                        <div class="tab-pane active show" id="tab-1">
                            <h3>Parenteral Drug Delivery</h3>
                            <img src="assets/img/tech_1.jpg" alt="" class="img-fluid">
                            <ul>
                                <li><i class="icofont-check-circled"></i> Controlled duration of drug delivery</li>
                                <li><i class="icofont-check-circled"></i> Controlled rate of drug delivery</li>
                                <li><i class="icofont-check-circled"></i> Complete bioerosion</li>
                                <li><i class="icofont-check-circled"></i> Simple/flexible fabrication</li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="tab-2">
                            <h3>Oral Drug Delivery</h3>
                            <img src="assets/img/tech_2.jpg" alt="" class="img-fluid">
                            <ul>
                                <li><i class="icofont-check-circled"></i> Site-specific drug delivery</li>
                                <li><i class="icofont-check-circled"></i> Reliable, sustained drug delivery</li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="tab-3">
                            <h3>Topical</h3>
                            <img src="assets/img/tech_3.jpg" alt="" class="img-fluid">
                            <ul>
                                <li><i class="icofont-check-circled"></i> Approved products</li>
                                <li><i class="icofont-check-circled"></i> Adaptable to a wide variety of substances</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
<!--</main>-->
<?php include './footer/footer.php'; ?>