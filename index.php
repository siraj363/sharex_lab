<?php

include './header/topbar.php';
include './header/header.php';
include_once './classes/Crud.php';


$query_cat = 'SELECT *
FROM categories';
$crud = new \Crud();
$categories = $crud->getData($query_cat);
?>
<!-- ======= Hero Section ======= -->
<section id="hero">
    <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide 1 -->
            <div class="carousel-item active" style="background-image: url(assets/img/slide1.jpg)">
                <div class="container">
                    <h2>Welcome to <span>Sharex Laboratories Ltd.</span></h2>
                    <p>SHAREX Laboratories Ltd. is a specialty pharmaceutical company focused on the development and commercialization of innovative medical treatments utilizing its proprietary drug delivery systems.</p>
                    <a href="about-us.php" class="btn-get-started scrollto">Read More</a>
                </div>
            </div>
            <!-- Slide 2 -->
            <div class="carousel-item" style="background-image: url(assets/img/slide2.jpeg)">
                <div class="container">
                    <h2>Production Activities</h2>
                    <p>Our factory is well equipped with modern machines and equipments to manufacture high quality Injectables, Tablets, Medicinal Syrups, Opthalmics and Galenicals..</p>
                    <a href="about-us.php" class="btn-get-started scrollto">Read More</a>
                </div>
            </div>
            <!-- Slide 3 -->
            <div class="carousel-item" style="background-image: url(assets/img/slide3.jpg)">
                <div class="container">
                    <h2>National Network</h2>
                    <p>We have our offices spreading all over the country and have an excellent net work of distribution.</p>
                    <a href="about-us.php" class="btn-get-started scrollto">Read More</a>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section><!-- End Hero -->
<main id="main">
    <section id="introduction-main" class="services about">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-12 border-right content">
                    <!--<marquee behavior="scroll" scrollamount="4" onmouseover="this.stop();" onmouseout="this.start();">-->
                        <div class="row">
                            <div class="col-4" style="padding: 0;">
                                <div class="iso-image-section" style="background-image: url('assets/img/9001_2000_small.jpg'); background-size: cover;background-repeat: no-repeat;background-position: center; height: 136px;"></div>
                            </div>
                            <div class="col-4" style="padding: 0px 2px 0px 2px;">
                                <div class="iso-image-section" style="background-image: url('assets/img/picture_2.jpg'); background-size: cover;background-repeat: no-repeat;background-position: center; height: 136px;"></div>
                            </div>
                            <div class="col-4" style="padding: 0px 2px 0px 2px;">
                                <div class="iso-image-section" style="background-image: url('assets/img/picture_4.jpg'); background-size: cover;background-repeat: no-repeat;background-position: center; height: 136px;"></div>
                            </div>
                        </div>
                    <!--</marquee>-->
                    <br>
                    <div class="row">
                        <div class="col-12">
                            <h4>Sharex at a Glance</h4>
                            <p>Highly versatile, patented technologies. Key advantages:</p>
                            <ul>
                                <li><i class="icofont-check-circled"></i> Controlled duration of drug delivery</li>
                                <li><i class="icofont-check-circled"></i> Controlled rate of drug delivery</li>
                                <li><i class="icofont-check-circled"></i> Complete bioerosion</li>
                                <li><i class="icofont-check-circled"></i> Simple/flexible fabrication</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <h4>History</h4>
                    <p>Sharex Laboratories was established in 1962 by two 
                        young and energetic Pharmacists with a merge capital 
                        of RS. 1,00,000/- (US $ 10,000) only. The main objective
                        was to manufacture and market products commonly required
                        by people at moderate prices with excellent quality.
                        As a result of our intensive efforts, we have been growing 
                        steadily and have established a prominent position in 
                        Pharmaceutical Industry in Pakistan. Now our authorised 
                        capital is RS. 2,00,00,000.00 ( US $ 20,00,000) and our 
                        position among Pakistan Pharmaceutical Manufacturers is 15th 
                        (Investment and Sales-wise) excluding 24 multinational joint 
                        ventures.</p>
                    <p>At present, we are marketing 54 registered products which are enjoying excellent sales in Pakistan. However, to take advantage of international research in pharmaceuticals, we are keeping close contacts with the prominent pharmaceuticals companies throughout the world and are going to introduce some latest research products in Pakistan shortly.</p>
                    <!--<h4>Present</h4>-->
                    
                    <h4>Sharex's Determination</h4>
                    <p>The Pharmaceutical Industry in Pakistan is facing a number of difficult problems such as intensification of governmental regulations, increase in developmental cost, period for registration of new drugs and strict price control. Sharex is determined to overcome these difficulties and to contribute towards the betterment in the health and welfare of mankind.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= Appointment Section ======= -->
<!--    <section id="appointment" class="appointment section-bg paddiing-top-sections">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Place an Order query</h2>
                <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
            </div>
            <form name="submit_order_form" action="submit_order.php" method="post">
                <div class="form-row">
                    <div class="col-md-4 form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
                        <div class="validate"></div>
                    </div>
                    <div class="col-md-4 form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email">
                        <div class="validate"></div>
                    </div>
                    <div class="col-md-4 form-group">
                        <input type="tel" class="form-control" name="phone" id="phone" placeholder="Your Phone" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
                        <div class="validate"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4 form-group">
                        <input type="text" name="address" class="form-control" id="address" placeholder="Address">
                        <div class="validate"></div>
                    </div>
                    <div class="col-md-4 form-group">
                        <select id="category" name="categories" class="form-control multiselect" multiple="" data-placeholder='Select Categories'>
                            <?php foreach ($categories as $cat) { ?>
                                <option value="<?= $cat['id'] ?>"><?= $cat['name'] ?></option>
                            <?php } ?>
                        </select>
                        <div class="validate"></div>
                    </div>
                    <div class="col-md-4 form-group">
                        <select name="products[]" id="products_multiselect" class="form-control multiselect" multiple="" data-placeholder='Select Products'>
                        </select>
                        <div class="validate"></div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                            <div class="validate"></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="text-center">
                            <input type="submit" name="submit" class="btn btn-primary" value="Proceed"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>-->
    <!-- End Appointment Section -->
</main><!-- End #main -->
<?php
include './footer/footer.php';
?>

