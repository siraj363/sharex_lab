<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 col-md-6">
                    <div class="footer-info">
                        <img style="width: 70px;" src="assets/img/sharex.png" alt="">
                        <!--<span style="font-size: 16px;color: black;font-weight: bold;" class="d-none d-lg-inline">SHAREX LABORATORIES</span>-->
                        <br/>
                        <p>
                            Sharex Laboratories Ltd., KLP Road, Sadiqabad<br>
                            District Rahimyar Khan, Pakistan<br><br>
                        </p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 footer-links footer-nav">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="about-us.php">About us</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="management.php">Our Management</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="branches.php">Branch Offices</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="bankers.php">Bankers</a></li>
                    </ul>
                </div>

                <!--                <div class="col-lg-6 col-md-6 footer-newsletter">
                                    <h4>Our Newsletter</h4>
                                    <p>Subscribe our newsletter to receive the latest news and exclusive offers every week. No spam.</p>
                                    <form action="" method="post">
                                        <input type="email" name="email"><input type="submit" value="Subscribe">
                                    </form>
                
                                </div>-->

            </div>
        </div>
    </div>
    <div class="modal" id="mcb_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">MCB Bank Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>0486-SADIQABAD MAIN BRANCH<br>SADIQABAD-DISTT. RAHIM YAR KHAN</p>
                    <p><b>Account#</b><br> PK79 MUCB 0048 6010 1000 1762<br><b>M/S</b><br>SHAREX LABORTRYES LTD.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="ubl_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">UBL Bank Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>GHALLA MANDI SADIQABAD<br>GHALLA MANDI, SADIQABAD, TEHSIL SAD</p>
                    <p><b>IBAN</b><br> PK09 UNIL 0112 0374 0100 1487<br>SHAREX LABORATORIES</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="bank_al_habib_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Bank AL Habib Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>SADIQABAD BRANCH</p>
                    <p><b>IBAN</b><br>PK65 BAHL 0025 0081 0000 7501<br>FAROOQ AMIN BAJWA</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="alfalah_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Bank Alflah Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>SADIQABAD BRANCH</p>
                    <p><b>ACC#</b><br>PK96ALFH0052001003453231<br>SHAREX LABORATORIES PVT. LIMITED</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="meezan_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Meezan Bank Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>(1901) SADIQABAD BRANCH<br>Sadiqabad - Pakistan</p>
                    <p>PK59 MEZN 0019 0101 0075 1672</p>
                    <p><b>ACC#</b><br>1901 0100751672<br>SHAREX LABORATORIES (PVT) LTD.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</footer><!-- End Footer -->

<div id="preloader"></div>
<a href="index.php#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="assets/js/notify.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>
<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="assets/vendor/counterup/counterup.min.js"></script>
<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="assets/vendor/venobox/venobox.min.js"></script>
<script src="assets/vendor/aos/aos.js"></script>

<!-- Template Main JS File -->

<script src="assets/js/main.js"></script>
<script src="assets/js/select2.min.js"></script>
<!--<script src="assets/js/bootstrap-multiselect.js"></script>-->
<!--<script src="assets/js/bootstrap-multiselect.js"></script>-->
<script src="assets/js/dataTables.js"></script>

<script async defer
        <!--src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC97QYvy2ypz0UW70CG9BqrVHUYW8fRFr8&callback=initMap">-->
</script>
<script>
    function initMap() {
        // The location of Uluru
        var uluru = {lat: 28.316601, lng: 70.128712};
        // The map, centered at Uluru
        var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 16, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }
</script>
</body>
</html>