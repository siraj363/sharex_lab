<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Sharex Laboratory</title>
        <meta content="" name="descriptison">
        <meta content="" name="keywords">
        <!-- Favicons -->
        <link href="assets/img/favicon_new.ico" rel="icon">
        <link href="assets/img/apple-touch.ico" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
        <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
        <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
        <link href="assets/vendor/aos/aos.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        
        <link href="assets/css/dataTables.css" rel="stylesheet">
        <!--<link href="assets/css/bootstrap-multiselect.css" rel="stylesheet">-->
        <link href="assets/css/select2.min.css" rel="stylesheet">
        <!--<link href="assets/css/bootstrap-multiselect.css" rel="stylesheet">-->
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body>
        <!-- ======= Top Bar ======= -->
        <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
            <div class="container d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <i class="icofont-clock-time"></i> Monday - Saturday, 07:30AM to 03:00PM
                </div>
                <div class="d-flex align-items-center">
                    <i class="icofont-envelope"></i> Mail: <a style="color: white;" href="mailto:sharexlabs@gmail.com">sharexlabs@gmail.com</a>
                </div>
            </div>
        </div>