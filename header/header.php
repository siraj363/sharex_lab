<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

        <a href="index.php" class="logo mr-auto">
            <!--<span style="font-size: 14px;color: black;" class="d-none d-lg-inline">SHAREX</span>-->
            <img src="assets/img/sharex.png" alt="">
            <span style="position: relative; bottom: 3px; display: inline; font-size: 14px;color: black;" class="">SHAREX LABORATORIES</span>
        </a>
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="<?= strpos($_SERVER['REQUEST_URI'], 'index.php') != false ? 'active' : ''?>"><a id="index" href="index.php">Home</a></li>
                <li class="<?= strpos($_SERVER['REQUEST_URI'], 'about-us.php') != false ? 'active' : ''?>"><a id="about-us" href="about-us.php">About Us</a></li>
                <li class="<?= strpos($_SERVER['REQUEST_URI'], 'products.php') != false ? 'active' : ''?>"><a id="products" href="products.php">Products</a></li>
                <li class="<?= strpos($_SERVER['REQUEST_URI'], 'platforms.php') != false ? 'active' : ''?>"><a id="platforms" href="platforms.php">Technologies & Platforms</a></li>
                <li class="<?= strpos($_SERVER['REQUEST_URI'], 'employment.php') != false ? 'active' : ''?>"><a id="employment" href="employment.php">Employment</a></li>
                <li class="<?= strpos($_SERVER['REQUEST_URI'], 'contact-us.php') != false ? 'active' : ''?>"><a id="contact-us" href="contact-us.php">Contact</a></li>

            </ul>
        </nav><!-- .nav-menu -->
        <!--<a href="index.php#appointment" class="appointment-btn scrollto"><span class="d-none d-md-inline">Place an</span> Order</a>-->

    </div>
</header><!-- End Header -->